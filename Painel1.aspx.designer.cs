﻿//------------------------------------------------------------------------------
// <gerado automaticamente>
//     Este código foi gerado por uma ferramenta.
//
//     As alterações ao arquivo poderão causar comportamento incorreto e serão perdidas se
//     o código for recriado
// </gerado automaticamente>
//------------------------------------------------------------------------------

namespace atividade02
{


    public partial class Painel1
    {

        /// <summary>
        /// Controle form1.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;

        /// <summary>
        /// Controle Panel1.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel Panel1;

        /// <summary>
        /// Controle Panel2.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel Panel2;

        /// <summary>
        /// Controle txtNome.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtNome;

        /// <summary>
        /// Controle txtbSobrenome.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtbSobrenome;

        /// <summary>
        /// Controle txtGenero.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtGenero;

        /// <summary>
        /// Controle txtbCelular.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtbCelular;

        /// <summary>
        /// Controle btn1proximo.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btn1proximo;

        /// <summary>
        /// Controle Panel3.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel Panel3;

        /// <summary>
        /// Controle txtbEndereço.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtbEndereço;

        /// <summary>
        /// Controle txtCidade.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtCidade;

        /// <summary>
        /// Controle txtbCidade.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtbCidade;

        /// <summary>
        /// Controle btn1Voltar.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btn1Voltar;

        /// <summary>
        /// Controle btn2Proximo.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btn2Proximo;

        /// <summary>
        /// Controle Panel4.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel Panel4;

        /// <summary>
        /// Controle TextBox1.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TextBox1;

        /// <summary>
        /// Controle TextBox2.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TextBox2;

        /// <summary>
        /// Controle btn2Voltar.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btn2Voltar;

        /// <summary>
        /// Controle BtnEnviar.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button BtnEnviar;

        /// <summary>
        /// Controle lblApresentaResultado.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblApresentaResultado;
    }
}
