﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace atividade02
{
    public partial class Painel1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Panel2.Visible = true;
            Panel3.Visible = false;
            Panel4.Visible = false;
        }

        protected void btn1proximo_Click(object sender, EventArgs e)
        {
            Panel2.Visible = false;
            Panel3.Visible = true;
            Panel4.Visible = false;
        }

        protected void btn1Voltar_Click(object sender, EventArgs e)
        {
            Panel2.Visible = true;
            Panel3.Visible = false;
            Panel4.Visible = false;
        }


        protected void btn2Voltar_Click(object sender, EventArgs e)
        {
            Panel2.Visible = false;
            Panel3.Visible = true;
            Panel4.Visible = false;
        }
        protected void btn2Proximo_Click(object sender, EventArgs e)
        {
                Panel2.Visible = false;
                Panel3.Visible = false;
                Panel4.Visible = true;
        }

        protected void BtnEnviar_Click(object sender, EventArgs e)
        {
            Panel2.Visible = false;
            Panel3.Visible = false;
            Panel4.Visible = true;
            lblApresentaResultado.Text = "AVISO! Os seu dados foram enviados com sucesso";
        }



    }
}