﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Painel1.aspx.cs" Inherits="atividade02.Painel1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
            <table style="width: 100%;" class="tblInfoPessoais">
                <tr>
                    <td>
                        <strong>Atividade 02 - Panel</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel1" runat="server">
                            <asp:Panel ID="Panel2" runat="server">
                                <table style="width: 100%;">
                                    <tr>
                                        <td colspan="2" style=" text-align:center">
                                            <strong>Informações Pessoais</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nome</td>
                                        <td><asp:TextBox ID="txtNome" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>Sobrenome</td>
                                        <td><asp:TextBox ID="txtbSobrenome" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>Genero</td>
                                        <td><asp:TextBox ID="txtGenero" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>Celular</td>
                                        <td><asp:TextBox ID="txtbCelular" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width:100px; height:26px "></td>
                                        <td><asp:Button ID="btn1proximo" runat="server" Text="Proximo" OnClick="btn1proximo_Click" /></td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="Panel3" runat="server">
                                <table style="width: 100%;">
                                    <tr>
                                        <td colspan="2" style=" text-align:center">
                                            <strong>Detalhes de endereço</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Endereço</td>
                                        <td><asp:TextBox ID="txtbEndereço" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>Cidade</td>
                                        <td><asp:TextBox ID="txtCidade" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>CEP</td>
                                        <td><asp:TextBox ID="txtbCidade" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr >
                                        <td style="width:100px; height:26px "></td>
                                        <td><asp:Button ID="btn1Voltar" runat="server" Text="Voltar" OnClick="btn1Voltar_Click" /></tdstyle="width:100px;>
                                    </tr>
                                    <tr >
                                        <td style="width:100px; height:26px "></td>
                                        <td><asp:Button ID="btn2Proximo" runat="server" Text="Proximo" OnClick="btn2Proximo_Click"  /></tdstyle="width:100px;>
                                    </tr>
                                </table>
                            </asp:Panel>

                              <asp:Panel ID="Panel4" runat="server">
                                <table style="width: 100%;">
                                    <tr>
                                        <td colspan="2" style=" text-align:center">
                                            <strong>Detalhes de endereço</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Login</td>
                                        <td><asp:TextBox ID="TextBox1" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>Senha</td>
                                        <td><asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox></td>
                                    </tr>
                                    <tr >
                                        <td style="width:100px; height:26px "></td>
                                        <td><asp:Button ID="btn2Voltar" runat="server" Text="Voltar" OnClick="btn2Voltar_Click" /></tdstyle="width:100px;>
                                    </tr>
                                    <tr >
                                        <td style="width:100px; height:26px "></td>
                                        <td><asp:Button ID="BtnEnviar" runat="server" Text="Enviar" OnClick="BtnEnviar_Click" /></tdstyle="width:100px;>
                                    </tr>
                                    <tr >
                                        <td style="width:100px; height:26px "></td>
                                        <td>
                                            <asp:Label ID="lblApresentaResultado" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                </tr>

                
            </table>
        <div>
        </div>
    </form>
</body>
</html>
